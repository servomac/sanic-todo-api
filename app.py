from sanic import Sanic
from sanic.response import json

from views.todo import todo_blueprint

app = Sanic(__name__)
app.blueprint(todo_blueprint)

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8000)
