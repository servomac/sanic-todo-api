import itertools

from typing import List, Dict

counter = itertools.count(1).__next__
TODOS = {}

async def list_all() -> List:
    def add_id(dictionary, id):
        return {**{'id': id}, **dictionary}

    return [add_id(todo, id) for id, todo in TODOS.items()]

async def add(new_todo) -> Dict:
    id = counter()
    TODOS[id] = new_todo

    created_todo = new_todo
    created_todo['id'] = id
    return created_todo

async def delete(id: int):
    del TODOS[id]

async def exists(id: int) -> bool:
    return id in TODOS

async def get(id: int) -> Dict:
    return TODOS[id]
