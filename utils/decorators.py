from functools import wraps

from sanic.response import json

def validate_json(f):
    @wraps(f)
    async def wrapper(request, *args, **kwargs):
        not_json_error = {'error': 'You should send JSON input!'}
        if request.json is None:
            return json(not_json_error, status=400)
        response = await f(request, *args, **kwargs)
        return response
    return wrapper
