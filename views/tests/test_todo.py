from json import loads, dumps

from app import app

def path(id=None):
    base_path = '/todo/'

    if id:
        return '{}{}'.format(base_path, id)
    return base_path

def create_todo(new_todo):
    """ Adds a todo and returns the id. """
    _, res = app.test_client.post(path(), data=dumps(new_todo))
    assert res.status == 201

    return loads(res.text)['id']

def test_todo_list_empty():
    _, res = app.test_client.get(path())
    assert res.status == 200
    assert loads(res.text) == []

def test_create_without_json_request():
    _, res = app.test_client.post(path())

    assert res.status == 400
    assert loads(res.text) == {'error': 'You should send JSON input!'}

def test_create_success():
    new_todo = {'title': 'title', 'description': 'desc'}
    expected_result = new_todo
    expected_result['id'] = 1

    _, res = app.test_client.post(path(), data=dumps(new_todo))
    assert res.status == 201
    assert loads(res.text) == expected_result

    _, res = app.test_client.get(path())
    assert loads(res.text) == [expected_result]

# TODO failure if there is not json data

def test_create_parameter_validation_failure():
    new_todo = {}
    required_error_msg = 'Missing data for required field.'
    expected_error = {
        'description': [required_error_msg],
        'title': [required_error_msg],
    }
    _, res = app.test_client.post(path(), data=dumps(new_todo))
    assert res.status == 422
    assert loads(res.text) == expected_error

def test_retrieve_non_existent_todo():
    id = 999
    _, res = app.test_client.get(path(id))

    assert res.status == 404
    assert loads(res.text) == {}

def test_retrieve_existent_todo():
    new_todo = {'description': 'desc', 'title': 'title'}
    new_todo_id = create_todo(new_todo)

    _, res = app.test_client.get(path(new_todo_id))
    assert res.status == 200
    assert loads(res.text) == {**{'id': new_todo_id}, **new_todo}

def test_delete_non_existent_todo():
    id = 999
    _, res = app.test_client.delete(path(id))

    assert res.status == 404
    assert loads(res.text) == {}

def test_delete_todo():
    new_todo = {'description': 'description', 'title': 'title'}
    new_todo_id = create_todo(new_todo)

    _, res = app.test_client.delete(path(new_todo_id))

    assert res.status == 204
    assert res.text == ''

    _, res = app.test_client.get(path())

    assert res.status == 200
    existent_ids = [todo['id'] for todo in loads(res.text)]
    assert new_todo_id not in existent_ids
