from sanic.response import json
from sanic import Blueprint

from entities import todo
from schemas.todo import TodoSchema
from utils.decorators import validate_json

todo_blueprint = Blueprint('todo', url_prefix='todo')

@todo_blueprint.route('/', methods=['GET'])
async def list_todos(request):
    return json(await todo.list_all())

@todo_blueprint.route('/', methods=['POST'])
@validate_json
async def create_todo(request):
    todo_schema = TodoSchema()
    input_todo, errors = todo_schema.load(request.json)

    if errors:
        return json(errors, status=422)

    created_todo = await todo.add(input_todo)
    return json(created_todo, status=201)

@todo_blueprint.route('/<id:int>', methods=['GET'])
async def show_todo_detail(request, id):
    if not await todo.exists(id):
        return json({}, status=404)

    return json(await todo.get(id), status=200)

@todo_blueprint.route('/<id:int>', methods=['DELETE'])
async def delete_todo(request, id):
    if not await todo.exists(id):
        return json({}, status=404)

    await todo.delete(id)
    return json({}, status=204)
